#!/bin/bash
echo "Setting up static build env...."

echo "//OpenCL files"> oclfiles.h
count=0
files=""
datas=""
sizes=""
for file  in  $(ls *.cl);
do
	if [ -f "$file" ]; then
		echo -n "Processing $file...."
		xxd -i "$file" >> oclfiles.h
		echo 'done'
		files="$files, \"$file\""
		trname=$(echo $file | tr \ .- _)
		datas="$datas, $trname"
		sizes="$sizes, &$trname"_len
		let count++
	fi
done
if [ $count -gt 0 ]; then
	echo >>oclfiles.h
	echo "const unsigned int CL_FILE_COUNT = $count;" >>oclfiles.h 
	echo "const char *CL_FILENAMES[]={${files:1}};" >>oclfiles.h
	echo "const unsigned char *CL_FILEDATA[]={${datas:1}};" >>oclfiles.h
	echo "const unsigned int *CL_FILESIZES[]={${sizes:1}};" >>oclfiles.h
	
fi
#exit

make clean > /dev/null
export CFLAGS="-DCURL_STATICLIB -DPTW32_STATIC_LIB"
export CPPFLAGS="-DCURL_STATICLIB -DPTW32_STATIC_LIB"
export LIBS="-lpdcurses -lpthreadGC2 -lws2_32 -lwinmm -lidn -lcrypto -lssl -lwldap32 -lssh2 -lrtmp -lz -lcrypto -lssl -lWs2_32 -lwinmm -lgdi32"
export LDFLAGS="-static"
./configure --enable-scrypt --enable-ztex --enable-modminer --enable-icarus  --enable-bitforce
make
